---
title: Monumente Funerare
subtitle:
comments: false
---

In general, monumentele funerare au urmatoarele dimensiuni: 120 cm inaltime x 55 cm latime x 8-10cm gromsime. Exista monumente cu dimensiuni mai mari de 120 cm, preturile fiind pe masura, deoarece cu cat materialul este mai mare -monumentul funerar este mai inalt- cu atat materialul este mai greu de gasit fara fisuri si crapaturi si implicit pretul mai creste.

 

FOARTE IMPORTANT!

 


Pe piata exista multi intermediari -in jur de aproximativ 90%-care vand monumente funerare din material de foarte slaba calitate, cel mai simplu mod de a afla este pretul exagerat de mic, dar desigur si structura materialului care se observa cu ochiul liber. Cum s-a ajuns in acesta situatie? Materialul care este cumparat din cariera se alege pe criteriul de cea mai slaba calitate, materiale care urmau a fi sparte pentru mozaicuri, cu fisuri si multa calcita, iar acesta este la un pret foarte mic si produsul finit va iesi la un pret mic dar in timp foarte scurt, acest material datorita variatiilor de temperatura si intemperiilor se degradeaza rapid, dupa 7-8 ani incercati sa faceti o plangere la OPC poate firma nu mai exista, aveti mult de umblat si mult stres, iar ca sa evitati aceasta este important sa ganditi astfel: sunt prea sarac pentru a-mi cumpara un produs ieftin”, chiar la renovarea unei camere de apartament sau casa pretul ajunge in jurul a 2000 – 2500 lei, cu atat mai mult pentru un monument funerar din marmura de calitatea I, pretul ar trebui sa fie cel putin in acest barem. Il comandati o singura data si trebuie gandit sa aibe o durabilitate de cel putin 100 ani fara a se interveni in reconditionare, pe de alta parte cu acest fel de gandire aratati si un mare respect fata de persoana care a trait sau pentru care doriti sa faceti acest monument, si chiar va respectati dumneavoastra. Daca faceti un mic calcul pentru un abonament la telefon platiti 30 lei/lunar, de ce nu s-ar plati 7 ani 30 lunar pentru o lucrare din marmura de foarte buna calitate in care sa nu mai aveti eventuale neplaceri? Daca totusi nu aveti aceasta suma mai bine mai asteptati si comandati. Materialul nostru este de calitate superioara, cu acte de provenienta de la firma certificata ISO specializata in extragerea si fasonarea marmurei cu scule profesionale fara a folosi scule rudimentare.

In general monumentele funerare sunt alcatuite din:

Postament sau soclu
2 Vaze sau lumanari
Corpul monumentului

Inainte de a comanda un monument sa va asigurati urmatoarele:

Ca aveti turnata fundatia pentru a se putea monta monumentul
V-ati ales corect modelul monumentului
Aveti textul complet care urmeaza a fi gravat pe monument
Daca doriti poza pe ceramica trebuie scanata la rezolutie 300 x 300 dpi si transmisa pe e-mail.

ASIGURAM TRANSPORTUL SI MONTAJUL in toata tara, in functie de comenzi pe acea zona.

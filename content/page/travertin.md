---
title: Travertin  
subtitle:
comments: false
---

Travertinul este o roca sedimentara alcatuita din calcit slab magnezian si aragonit. Travertinul se foloseste foarte mult in constructii chiar daca este o roca poroasa (porii se inchid cu ciment) si se slefuiesc avand luciul similar al marmurei.

Cateva caracteristici tehnice:
Densitate 2,1-2,3 tone pe metru cub -adica un bloc de 1 m inaltime x 1m latime x 1m grosime cantereste aproximativ 2100 kg
Absortie apa 1-1,2%
Scala lui Mohs   4-5  pe intelesul tuturor, a existat o persoana care a facut o evaluare a rocilor din punct de vedere al duritatii acestora. Cel mai putin dur ar fi talc-ul 1 pe scala Mohs si cel mai dur diamantul 10 iar pentru mai multe detalii intrati pe wikipedia.

Travertinul se extrage din cariere  in forma unor blocuri care apoi se debiteaza la diferite dimensiuni.
Travertinul se foloseste la placarea peretilor atat in interior cat si pe exterior. Poate fi folosit la placatul pardoselilor interioare si exterioare.

Travertinul se debiteaza la  diverse dimensiuni 15cm x 15cm, 30cm x 30cm, 40cm x 40cm, 60cm x 40cm sau  lungimi libere : intre 40cm x 0,8m si  40cm x 130cm lateralele fiind neregulate.

Travertin nechituit / neslefuit -rosturile raman vizibile, travertinul are o suprafata mai poroasa. Acest tip de placaj se foloseste, in general pentru  exterior si anume pavare terase, curti, trotuare etc. -se asigura o aderenta ridicata prin ramanerea suprafetei nelustruita

